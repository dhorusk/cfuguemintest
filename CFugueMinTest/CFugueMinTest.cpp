#include <iostream>
#include "CFugueLib.h"

void main()
{
	if (CFugue::GetMidiOutPortCount() <= 0)
	{
		std::cerr << "No MIDI Output Ports found!";
		exit(-1);
	}

	CFugue::PlayMusicString(L"C C X[VOLUME_COARSE]=50 C C");
}